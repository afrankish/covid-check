const CronJob = require('cron').CronJob;
const mailgun = require("mailgun-js");
const DOMAIN = 'mg.popupmagik.com';
const url = 'https://am-i-eligible.covid19vaccine.health.ny.gov/api/list-providers';
const mg = mailgun({apiKey:'key-b55a9e2b8fd5aeeffe5170fa8c1ca237', domain: DOMAIN});
var axios = require('axios')
const vaccineURL = 'https://am-i-eligible.covid19vaccine.health.ny.gov/';


var job = new CronJob('*/30 * * * * *', function() {
  
  console.log('You will see this message every 30 seconds');
  try{
    async function vaccineCheck(){
      var list = await axios.get(url);
      var data = list.data.providerList
      var avail = false;
      var loc = []
      for(let i = 0; i< data.length; i++){
        if(data[i].providerId == '1018'){
          if( data[i].availableAppointments == 'Y'){
            avail = true;
            loc.push(`<br> ${data[i].providerName} `)
          } 
          else console.log(`No appt at ${data[i].providerName}`)
        }
        if(data[i].providerId == '1011') {
          if( data[i].availableAppointments == 'Y'){ 
            avail = true;
            loc.push(`<br> ${data[i].providerName} `)
          }
          else console.log(`No appt at ${data[i].providerName}`)
        }
      }
      
      if(avail == true){
        const data = {
          from: 'noreply@mg.popupmagik.com',
          to: 'nuandy@gmail.com',
          subject: 'Covid Tests Available',
          html:`
            Appointments available at:
            <b>${loc}</b>
            `
        };
        mg.messages().send(data, function (error, body) {
          // if (error) throw 'An error occurred while sending your email'
              // console.log('mail', body);
        });
        job.stop()
        }
    }
    vaccineCheck()
  
  }catch(err){
  }
  
}, null, true, 'America/Los_Angeles');
job.start();